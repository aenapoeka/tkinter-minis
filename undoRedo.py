from tkinter import *
from tkinter import messagebox

# Lets you write and track number values with undo and redo functionality.

class UndoRedo:
    def __init__(self, main):
        main.title("Excercise 4.3")
        self.main = main

        self.numVar = StringVar()

        # History lists for undo & redo
        self.numberHistory = []
        self.numberFuture = []

        # Content to main
        content = Frame(main, padx = 15, pady = 15, bg = "orange")
        content.grid(row = 0, column = 0)
        
        self.numberSlider = Scale(content, from_=0, to=10, orient = HORIZONTAL)
        self.numberSlider.grid(row = 0, column = 0)

        self.numberButton = Button(content, text = "Add", command = self.addNumber, pady = 10)
        self.numberButton.grid(row = 0, column = 1)

        self.numberLabel = Label(content, textvariable = self.numVar, pady = 12, padx = 12)
        self.numberLabel.grid(row = 0, column = 2)

        # Creating submenus
        menubar = Menu(main)
        filemenu = Menu(master=menubar, tearoff=0)
        filemenu.add_command(label = "Quit", command = self.quit)
        
        editmenu = Menu(master=menubar, tearoff=0)
        editmenu.add_command(label = "Undo", command = self.undo)
        editmenu.add_command(label = "Redo", command = self.redo)

        # Submenus to menubar
        menubar.add_cascade(label = "File", menu = filemenu)
        menubar.add_cascade(label = "View", menu = editmenu)

        # Menu to root
        main.config(menu = menubar)

        main.protocol("WM_DELETE_WINDOW", self.quit)

    # Appends a number from scale to both future and history.
    # Then inserts it to a label
    def addNumber(self):
        num = str(self.numberSlider.get())
        self.numberHistory.append(num)
        self.numberFuture.append(num)
        newNumVar = " ".join(self.numberHistory)
        self.numVar.set(newNumVar)

    # Undoes work history
    def undo(self):
        # If there is something to undo in history
        if len(self.numberHistory) > 0:
            self.numberHistory.pop()
            newNumVar = " ".join(self.numberHistory)
            self.numVar.set(newNumVar)

    # Redoes work history
    def redo(self):
        # If there is history to be redone
        if len(self.numberHistory) < len(self.numberFuture):
            self.numberHistory.append(self.numberFuture[len(self.numberHistory)])
            newNumVar = " ".join(self.numberHistory)
            self.numVar.set(newNumVar)

    def quit(self):
        if messagebox.askyesno("Quit", "Are you sure you want to quit?"):
            self.main.quit()


root = Tk()
app = UndoRedo(root)
root.mainloop()