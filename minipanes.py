from tkinter import *
from functools import partial
from tkinter import messagebox

# The user can open multiple tiny textboxes next to each other.

class Minipanes:
    def __init__(self, master):
        self.master = master
        master.title("Infinite mini panes")

        # Value for dynamic widget variable names
        self.currentPanes = 1

        # Menubar and dropdowns
        menubar = Menu(master)
        fileMenu = Menu(menubar, tearoff=0)
        self.paneMenu = Menu(fileMenu, tearoff=0)
        fileMenu.add_command(label = "Add", command = self.addPane)
        fileMenu.add_cascade(label = "Close", menu = self.paneMenu)
        fileMenu.add_command(label = "Quit", command = self.quit)
        menubar.add_cascade(label = "File", menu = fileMenu)
        master.config(menu=menubar)

        # Paned window for text widget panes
        self.paned = PanedWindow(master, height=150, orient=HORIZONTAL, relief=RIDGE)
        self.paned.pack(fill=BOTH, expand=1)
        textPane = Text(self.paned, width=20, relief=RIDGE)
        self.paned.add(textPane)

        # Messagebox to quit
        master.protocol("WM_DELETE_WINDOW", self.quit)


    # Adds new panes to parent window
    def addPane(self):
        # Increasing dynamic variable name value
        self.currentPanes += 1

        # Initialising a new variable for the new widget
        newName = "Pane"+str(self.currentPanes)

        # Assigning dynamic variable to a new widget
        locals()[newName] = Text(self.paned, width=20, relief=RIDGE)
        self.paned.add(locals()[newName])
        self.paneMenu.add_command(label = (locals()[newName]), command = partial(self.closePane, locals()[newName]))

    # Removes given child from parent window and pane menu dropdown
    def closePane(self, paneName):
        self.paned.remove(paneName)
        self.paneMenu.delete(paneName)

    # Messagebox to menu quit
    def quit(self):
        if messagebox.askyesno("Exit program", "Do you really want to quit?"):
            self.master.quit()

root = Tk()
app = Minipanes(root)
root.mainloop()