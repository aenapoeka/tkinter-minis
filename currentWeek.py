from tkinter import *
import datetime

# Renders a tiny window that shows the current week.

class CurrentWeek:

    def __init__(self, master):
        # Gets user's monitors dimensions
        screenWidth = root.winfo_screenwidth()
        screenHeight = root.winfo_screenheight()

        # Dimensions for app
        w = 100
        h = 100

        # Placement positions on the user's screen
        xPosition = screenWidth - w
        yPosition = screenHeight/1.51 - h
        
        # Setting up application data
        master.title("Current Week")
        master.config(bg = "orange")
        master.geometry('%dx%d+%d+%d' % (w, h, xPosition, yPosition))
        master.config(padx = 15, pady = 15, borderwidth = 3, relief = RAISED)

        # Getting current week and placing it to a variable
        self.today = datetime.datetime.now()
        self.today = self.today.strftime("%W")
        weekVar = StringVar()
        weekVar.set(self.today)

        # Widgets for displaying data
        header = Label(master, text = "Week:", bg = "orange")
        header.grid(row = 0, column = 0)
        header.config(font = ("Helvetica", 9))
        weekLabel = Label(master, textvariable = weekVar)
        weekLabel.grid(row = 1, column = 0)
        weekLabel.config(font = ("Helvetica", 42), bg = "orange")


root = Tk()
app = CurrentWeek(root)
root.mainloop()