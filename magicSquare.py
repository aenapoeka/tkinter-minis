from tkinter import *
from tkinter import messagebox

# User can check if input values form a magic square
# https://en.wikipedia.org/wiki/Magic_square

class MagicSquare:

    def __init__(self, main):
        main.title("Magic Square")
        #main.config(bg = "orange")
        #main.geometry("300x200")

        # Main content frame for grid and button frames
        mainframe = Frame(main, padx = 75, pady = 15, bg = "orange")
        mainframe.grid(row = 0, column = 0)
        mainframe.rowconfigure(1, pad = 15)

        # Content frame for widgets
        content = Frame(mainframe, padx = 15, pady = 15)
        content.grid(row = 0, column = 0)

        # Adding widgets to content

        header = Label(content, text = "Magic Square")
        header.grid(row = 0, column = 0, columnspan = 3)

        self.row11 = Entry(content, width = 3, justify = 'right')
        self.row11.grid(row = 1, column = 0)
        self.row12 = Entry(content, width = 3, justify = 'right')
        self.row12.grid(row = 1, column = 1)
        self.row13 = Entry(content, width = 3, justify = 'right')
        self.row13.grid(row = 1, column = 2)

        self.row21 = Entry(content, width = 3, justify = 'right')
        self.row21.grid(row = 2, column = 0)
        self.row22 = Entry(content, width = 3, justify = 'right')
        self.row22.grid(row = 2, column = 1)
        self.row23 = Entry(content, width = 3, justify = 'right')
        self.row23.grid(row = 2, column = 2)

        self.row31 = Entry(content, width = 3, justify = 'right')
        self.row31.grid(row = 3, column = 0)
        self.row32 = Entry(content, width = 3, justify = 'right')
        self.row32.grid(row = 3, column = 1)
        self.row33 = Entry(content, width = 3, justify = 'right')
        self.row33.grid(row = 3, column = 2)


        magicButton = Button(mainframe, text = "Check!", command = self.magic)
        magicButton.grid(row = 1, column = 0)
    
    # Checks whether the square indeed is magical
    def magic(self):

        # Declaring integer variables for all entry fields
        iv11 = IntVar()
        iv12 = IntVar()
        iv13 = IntVar()

        iv21 = IntVar()
        iv22 = IntVar()
        iv23 = IntVar()

        iv31 = IntVar()
        iv32 = IntVar()
        iv33 = IntVar()

        # Getting filled values from fields to the variables
        iv11 = self.row11.get()
        iv12 = self.row12.get()
        iv13 = self.row13.get()

        iv21 = self.row21.get()
        iv22 = self.row22.get()
        iv23 = self.row23.get()

        iv31 = self.row31.get()
        iv32 = self.row32.get()
        iv33 = self.row33.get()

        # Checking if all values are unique

        varList = [iv11, iv12, iv13, iv21, iv22, iv23, iv31, iv32, iv33]

        if len(set(varList)) == 9:
            #print("Numbers are all unique")

            # Check rows by comparison (this part is lazy lol)
            if int(iv11) + int(iv12) + int(iv13) == int(iv21) + int(iv22) + int(iv23):
                if int(iv21) + int(iv22) + int(iv23) == int(iv31) + int(iv32) + int(iv33):
                    if int(iv11) + int(iv21) + int(iv31) == int(iv12) + int(iv22) + int(iv32):
                        if int(iv12) + int(iv22) + int(iv32) == int(iv13) + int(iv23) + int(iv33):
                            if int(iv11) + int(iv22) + int(iv33) == int(iv31) + int(iv22) + int(iv13):
                                #print("All rows produce", int(iv11)+int(iv12)+int(iv13))

                                messagebox.showinfo("Magic Check", "Your box is magical!")
            else:
                messagebox.showwarning("Magic Check", "Your box is not magical.")



root = Tk()
app = MagicSquare(root)
root.mainloop()
